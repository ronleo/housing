from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Flat)
admin.site.register(Rates)
admin.site.register(Bill)
admin.site.register(Complain)
admin.site.register(Notice)