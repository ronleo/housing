from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Flat(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    flat_no = models.IntegerField()
    area_sqft = models.IntegerField()
    cars = models.IntegerField(blank=True,null=True)
    bike = models.IntegerField(blank=True,null=True)
    email = models.EmailField(max_length=255,blank=True,null=True)
    mob_no = models.CharField(max_length=10,blank=True,null=True)

    def __str__(self):
        return str(self.flat_no)

class Rates(models.Model):
    persqft = models.IntegerField()
    perbike = models.IntegerField()
    percar  = models.IntegerField()
    water = models.IntegerField()
    penelty = models.IntegerField()


class Bill(models.Model):
    flat = models.ForeignKey(Flat,on_delete=models.CASCADE)
    carparking = models .IntegerField()
    bikeparking = models.IntegerField()
    water = models.IntegerField()
    maintainance = models.IntegerField()
    fine = models.IntegerField()
    date = models.DateField()
    month = models.IntegerField()
    year = models.IntegerField()
    paid = models.BooleanField(default=False)
    utrno = models.CharField(max_length=255,blank=True,null=True)
    total = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return str(self.flat)+'-'+str(self.date)


class Complain(models.Model):
    subject = models.CharField(max_length=255)
    complain = models.TextField()
    date = models.DateField()
    madeby = models.ForeignKey(User,on_delete=models.CASCADE)
    closed = models.BooleanField(default=False)

    def __str__(self):
        return self.subject


class Notice(models.Model):
    subject = models.CharField(max_length=255)
    notice = models.TextField()
    date = models.DateField()
    to = models.ManyToManyField(Flat,blank=True,null=True)
    published = models.BooleanField(default=True)

    def __str__(self):
        return self.subject