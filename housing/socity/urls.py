from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
from . import views


urlpatterns = [
    url(r'^addflat/$', views.addflat,name='addflat'),
    url(r'^flatlist/$', views.flatlist,name='flatlist'),
    url(r'^editflat/(?P<id>\d+)/$', views.editflat,name='editflat'),
    url(r'^delete/(?P<id>\d+)/$', views.delete,name='delete'),
    url(r'^genbill/(?P<id>\d+)/$', views.genbill,name='genbill'),
    url(r'^mybills/$', views.mybills,name='mybills'),
    url(r'^viewandpay/(?P<id>\d+)/$', views.viewandpay,name='viewandpay'),
    url(r'^checkbills/$', views.checkbills,name='checkbills'),
    url(r'^updatebillstatus/(?P<id>\d+)/$', views.updatebillstatus,name='updatebillstatus'),
    url(r'^complain/$', views.complain,name='complain'),
    url(r'^complainlist/$', views.complainlist,name='complainlist'),
    url(r'^complainlistadmin/$', views.complainlistadmin,name='complainlistadmin'),
    url(r'^read/(?P<id>\d+)/$', views.read,name='read'),
    url(r'^closecomplain/(?P<id>\d+)/$', views.closecomplain,name='closecomplain'),
    url(r'^addnotice/$', views.addnotice,name='addnotice'),
    url(r'^noticelistadmin/$', views.noticelistadmin,name='noticelistadmin'),
    url(r'^noticelist/$', views.noticelist,name='noticelist'),
    url(r'^closenotice/(?P<id>\d+)/$', views.closenotice,name='closenotice'),
    url(r'^readnotice/(?P<id>\d+)/$', views.readnotice,name='readnotice'),
    ]