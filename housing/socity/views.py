from django.shortcuts import render
from django.contrib.auth import authenticate, login,logout
from django.shortcuts import redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .permissions import  *
from .models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import date
from django.http import HttpResponse

# Create your views here.

def loginuser(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            complainscount = Complain.objects.filter(closed=False).count()
            members = Flat.objects.all().count()
            return render(request, 'common/dashboard_god.html', {'complainscount':complainscount,'members':members})
        else:
            flat = Flat.objects.get(user=request.user)
            pendingbills = Bill.objects.filter(flat=flat,paid=False).count()
            noticecount = Notice.objects.filter(to=flat,published=True).count()
            return render(request, 'common/dashboard.html', {'pendingbills':pendingbills,'noticecount':noticecount})
    elif request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # messages.success(request, 'You have successfully logged in')
            if user.is_superuser:
                complainscount = Complain.objects.filter(closed=False).count()
                members = Flat.objects.all().count()
                return render(request, 'common/dashboard_god.html', {'complainscount':complainscount,'members':members})
            else:
                flat = Flat.objects.get(user=request.user)
                pendingbills = Bill.objects.filter(flat=flat, paid=False).count()
                noticecount = Notice.objects.filter(to=flat, published=True).count()
                return render(request, 'common/dashboard.html', {'pendingbills':pendingbills,'noticecount':noticecount})
            # # msg = 'logged in successfully'
            # return render(request, 'dashboard.html', {'msg':msg})
        else:
            messages.error(request, 'Wrong Credentials')
            return render(request, 'common/login.html', {})
    else:
        return render(request, 'common/login.html', {})


def logout_user(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect('login')


@login_required(login_url='login')
@superuser_only
def addflat(request):
    if request.method == 'POST':
        user = User.objects.create(username=request.POST.get('phone'),first_name=request.POST.get('firstname'),
                                   last_name=request.POST.get('lastname'),password=request.POST.get('phone'))
        user.set_password(request.POST.get('phone'))
        if request.POST.get('email') != '':
            user.email = request.POST.get('email')
        user.save()
        flat = Flat.objects.create(user=user,flat_no=request.POST.get('flatno'),
                                   area_sqft=request.POST.get('sqft'),mob_no=request.POST.get('phone'))
        if request.POST.get('cars') != '':
            flat.cars = request.POST.get('cars')
        if request.POST.get('bikes') != '':
            flat.bike = request.POST.get('bikes')
        if request.POST.get('email') != '':
            flat.email = request.POST.get('email')
        flat.save()
        return redirect('flatlist')
    else:
        return render(request, 'common/addflat.html', {})


@login_required(login_url='login')
def flatlist(request):
    allflats = Flat.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(allflats, 5)
    try:
        members = paginator.page(page)
    except PageNotAnInteger:
        members = paginator.page(1)
    except EmptyPage:
        members = paginator.page(paginator.num_pages)
    return render(request, 'common/flatlist.html', {'members':members})


@login_required(login_url='login')
@superuser_only
def editflat(request,id):
    if request.method == "POST":
        Flat.objects.filter(id=id).update(flat_no=request.POST.get('flatno'),
                                          area_sqft=request.POST.get('sqft'),mob_no=request.POST.get('phone'),
                                          cars=request.POST.get('cars'),bike=request.POST.get('bikes'),
                                          email=request.POST.get('email'))
        flat = Flat.objects.get(id=id)
        user = User.objects.filter(id=flat.user.id).update(first_name=request.POST.get('firstname'),
                                                           last_name=request.POST.get('lastname'),
                                                           email=request.POST.get('email'))
        return redirect('flatlist')
    else:
        flat = Flat.objects.get(id=id)
        return render(request,'common/addflat.html', {'flat':flat})


@login_required(login_url='login')
@superuser_only
def delete(request,id):
    flat = Flat.objects.get(id=id)
    flat.user.delete()
    return redirect('flatlist')


@login_required(login_url='login')
@superuser_only
def genbill(request,id):
    flat = Flat.objects.get(id=id)
    rates =Rates.objects.get(id=1)
    today = date.today()
    try:
        bill = Bill.objects.get(flat=flat, month=today.month, year=today.year)
        messages.error(request, 'Bill Already Exists')
        return redirect('flatlist')
    except:
        carparking = flat.cars * rates.percar
        bikeparking = flat.bike * rates.perbike
        water = rates.water
        try:
            prevbill = Bill.objects.get(flat=flat, month=(int(today.month)-1), year=today.year)
            if prevbill.paid == False:
                fine=rates.penelty
            else:
                fine=0
        except:
            fine=0
        maintainance = flat.area_sqft * rates.persqft
        total = carparking+bikeparking+water+maintainance+fine
        bill = Bill.objects.create(flat=flat, month=today.month, year=today.year,date=today,carparking=carparking,
                                   bikeparking=bikeparking,water=water,maintainance=maintainance,fine=fine,total=total)
        messages.success(request, 'Bill Generated')
        return redirect('flatlist')


@login_required(login_url='login')
@nonstaff
def mybills(request):
    flat = Flat.objects.get(user=request.user)
    bills = Bill.objects.filter(flat=flat)
    page = request.GET.get('page', 1)
    paginator = Paginator(bills, 5)
    try:
        pagebills = paginator.page(page)
    except PageNotAnInteger:
        pagebills = paginator.page(1)
    except EmptyPage:
        pagebills = paginator.page(paginator.num_pages)
    return render(request, 'common/billslist.html', {'bills': pagebills})


@login_required(login_url='login')
@nonstaff
def viewandpay(request,id):
    flat = Flat.objects.get(user=request.user)
    bill = Bill.objects.get(id=id)
    rates = Rates.objects.get(id=1)
    if request.method == 'POST':
        bill.utrno = request.POST.get('utr')
        bill.save()
        return redirect('mybills')
    else:
        return render(request, 'common/billpay.html', {'bill': bill,'flat':flat,'rates':rates})


@login_required(login_url='login')
@superuser_only
def checkbills(request):
    bills = Bill.objects.filter(paid=False).exclude(utrno=None)
    page = request.GET.get('page', 1)
    paginator = Paginator(bills, 5)
    try:
        pagebills = paginator.page(page)
    except PageNotAnInteger:
        pagebills = paginator.page(1)
    except EmptyPage:
        pagebills = paginator.page(paginator.num_pages)
    return render(request, 'common/adminbillslist.html', {'bills': pagebills})


@login_required(login_url='login')
@superuser_only
def updatebillstatus(request,id):
    bill = Bill.objects.get(id=id)
    if request.GET.get('approved') == '1':
        bill.paid =True
    else:
        bill.utrno = None
    bill.save()
    return redirect('checkbills')


@login_required(login_url='login')
@nonstaff
def complain(request):
    if request.method == 'POST':
        complain = Complain.objects.create(subject=request.POST.get('subject'),complain=request.POST.get('complain'),
                                           madeby=request.user,date=date.today())
        return redirect('complainlist')
    else:
        return render(request, 'common/complain.html', {})


@login_required(login_url='login')
@nonstaff
def complainlist(request):
    complains = Complain.objects.filter(madeby=request.user)
    page = request.GET.get('page', 1)
    paginator = Paginator(complains, 10)
    try:
        pagecomplains = paginator.page(page)
    except PageNotAnInteger:
        pagecomplains = paginator.page(1)
    except EmptyPage:
        pagecomplains = paginator.page(paginator.num_pages)
    return render(request, 'common/complainslist.html', {'complains': pagecomplains})


@login_required(login_url='login')
@superuser_only
def complainlistadmin(request):
    complains = Complain.objects.filter(closed=False)
    page = request.GET.get('page', 1)
    paginator = Paginator(complains, 10)
    try:
        pagecomplains = paginator.page(page)
    except PageNotAnInteger:
        pagecomplains = paginator.page(1)
    except EmptyPage:
        pagecomplains = paginator.page(paginator.num_pages)
    return render(request, 'common/complainlistadmin.html', {'complains': pagecomplains})


@login_required(login_url='login')
@superuser_only
def read(request,id):
    complain = Complain.objects.get(id=id)
    msg = complain.complain
    return HttpResponse(msg)


@login_required(login_url='login')
@superuser_only
def closecomplain(request,id):
    complain = Complain.objects.get(id=id)
    complain.closed = True
    complain.save()
    return redirect('complainlistadmin')


@login_required(login_url='login')
@superuser_only
def addnotice(request):
    if request.method == "POST":
        notice = Notice.objects.create(subject=request.POST.get('subject'),notice=request.POST.get('notice'),
                                           date=date.today())
        tolist = request.POST.getlist('to')
        for each in tolist:
            flat = Flat.objects.get(id=each)
            notice.to.add(flat)
            notice.save()
        return redirect('noticelistadmin')
    else:
        allflats = Flat.objects.all()
        return render(request, 'common/addnotice.html',{'allflats':allflats})


@login_required(login_url='login')
@superuser_only
def noticelistadmin(request):
    notices = Notice.objects.filter(published=True)
    page = request.GET.get('page', 1)
    paginator = Paginator(notices, 10)
    try:
        pagenotices = paginator.page(page)
    except PageNotAnInteger:
        pagenotices = paginator.page(1)
    except EmptyPage:
        pagenotices = paginator.page(paginator.num_pages)
    return render(request, 'common/noticelistadmin.html', {'notices': pagenotices})


@login_required(login_url='login')
@superuser_only
def closenotice(request,id):
    notice = Notice.objects.get(id=id)
    notice.published = False
    notice.save()
    return redirect('noticelistadmin')


@login_required(login_url='login')
@nonstaff
def noticelist(request):
    flat = Flat.objects.get(user=request.user)
    notices = Notice.objects.filter(published=True,to=flat)
    page = request.GET.get('page', 1)
    paginator = Paginator(notices, 10)
    try:
        pagenotices = paginator.page(page)
    except PageNotAnInteger:
        pagenotices = paginator.page(1)
    except EmptyPage:
        pagenotices = paginator.page(paginator.num_pages)
    return render(request, 'common/noticelist.html', {'notices': pagenotices})


@login_required(login_url='login')
@nonstaff
def readnotice(request,id):
    notice = Notice.objects.get(id=id)
    msg = notice.notice
    return HttpResponse(msg)